import React from 'react';
import {Channel} from "../components/Channel/Channel";

import first from '../assets/images/first.svg';
import twoAndTwo from '../assets/images/2-2.svg';
import rbk from '../assets/images/rbk.svg';
import amedia from '../assets/images/amedia.svg';
import {TabMenu} from "../components/TabMenu/TabMenu";

export const ChannelsPage = () => {

  const channels = [
    {
      title: 'Первый канал',
      logo: first,
      items: [{time: '13:00', name: 'Новости (с&nbsp;субтитрами)'}, {
        time: '14:00',
        name: 'Давай поженимся'
      }, {time: '15:00', name: 'Другие новости'}]
    },
    {
      title: '2x2',
      logo: twoAndTwo,
      items: [{time: '13:00', name: 'МУЛЬТ ТВ. Сезон&nbsp;4, 7&nbsp;серия'}, {
        time: '14:00',
        name: 'ПОДОЗРИТЕЛЬНАЯ СОВА. Сезон&nbsp;7, 7&nbsp;серия'
      }, {time: '15:00', name: 'БУРДАШЕВ. Сезон&nbsp;1, 20&nbsp;серия'}]
    },
    {
      title: 'РБК',
      logo: rbk,
      items: [{time: '13:00', name: 'ДЕНЬ. Горючая смесь: как бороться с&nbsp;суррогатом на&nbsp;АЗС'}, {
        time: '14:00',
        name: 'ДЕНЬ. Главные темы'
      }, {time: '15:00', name: 'Главные новости'}]
    },
    {
      title: 'AMEDIA PREMIUM',
      logo: amedia,
      items: [{time: '13:00', name: 'Клиент всегда мёртв'}, {
        time: '14:00',
        name: 'Голодные игры: Сойка-пересмешница. Часть&nbsp;I'
      }, {time: '15:00', name: 'Секс в&nbsp;большом городе'}]
    },
  ];

  return (
    <div className='channels'>
      <TabMenu page='channels'/>
      <section className="channels__content">
        {channels.map((item, index) => {
          return (
            <Channel className='channels__item' key={index} title={item.title} logo={item.logo} items={item.items}/>
          )
        })}
      </section>
    </div>
  );
};
