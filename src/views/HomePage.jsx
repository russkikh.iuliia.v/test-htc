import React from 'react';

import movie1 from '../assets/images/movie-1.jpg';
import movie2 from '../assets/images/movie-2.jpg';
import movie3 from '../assets/images/movie-3.jpg';
import movie4 from '../assets/images/movie-4.jpg';
import {MovieCard} from "../components/MovieCard/MovieCard";
import {Genre} from "../components/Genre/Genre";
import {TabMenu} from "../components/TabMenu/TabMenu";

export const HomePage = () => {
  const movies = [
    {
      alt: 'Мульт вкино. Выпуск №103. Некогда грустить!',
      title: 'Мульт в&nbsp;кино. Выпуск №103. Некогда грустить!',
      img: movie1,
      description: 'Фильм повествует о&nbsp;череде событий, произошедших в&nbsp;Голливуде в&nbsp;1969&nbsp;году, на&nbsp;закате его &laquo;золотого века&raquo;. Известный актер Рик Далтон и&nbsp;его дублер Клифф Бут пытаются найти свое место в&nbsp;стремительно меняющемся мире киноиндустрии.'
    },
    {
      alt: 'Новый Бэтмен',
      title: 'Новый Бэтмен',
      img: movie2,
      description: 'Фильм повествует о&nbsp;череде событий, произошедших в&nbsp;Голливуде в&nbsp;1969&nbsp;году, на&nbsp;закате его &laquo;золотого века&raquo;. Известный актер Рик Далтон и&nbsp;его дублер Клифф Бут пытаются найти свое место в&nbsp;стремительно меняющемся мире киноиндустрии.'
    },
    {
      alt: 'Однажды... вГолливуде',
      title: 'Однажды... в&nbsp;Голливуде',
      img: movie3,
      description: 'Фильм повествует о&nbsp;череде событий, произошедших в&nbsp;Голливуде в&nbsp;1969&nbsp;году, на&nbsp;закате его &laquo;золотого века&raquo;. Известный актер Рик Далтон и&nbsp;его дублер Клифф Бут пытаются найти свое место в&nbsp;стремительно меняющемся мире киноиндустрии.'
    },
    {
      alt: 'Стриптизёрши',
      title: 'Стриптизёрши',
      img: movie4,
      description: 'Фильм повествует о&nbsp;череде событий, произошедших в&nbsp;Голливуде в&nbsp;1969&nbsp;году, на&nbsp;закате его &laquo;золотого века&raquo;. Известный актер Рик Далтон и&nbsp;его дублер Клифф Бут пытаются найти свое место в&nbsp;стремительно меняющемся мире киноиндустрии.'
    },
  ];

  const genres = [
    {emoji: '😁', kind: 'Комедии', bgColor: 'yellow'},
    {emoji: '😭', kind: 'Драмы', bgColor: 'orange'},
    {emoji: '👽', kind: 'Фантастика', bgColor: 'blue'},
    {emoji: '👻', kind: 'Ужасы', bgColor: 'gray'},
  ];

  return (
    <div className='home'>
      <TabMenu page='home'/>
      <div className="home__content">
        <section className="home__section">
          <span className="home__title">🔥 Новинки</span>
          <div className="home__gallery">
            {movies.map((item, index) => {
              return (
                <MovieCard title={item.title} img={item.img} description={item.description} key={index}/>
              )
            })}
          </div>
        </section>
        <section className="home__section">
          <span className="home__title">Жанры</span>
          <div className="home__gallery">
            {genres.map((item, index) => {
              return (
                <Genre key={index} emoji={item.emoji} kind={item.kind} bgColor={item.bgColor}/>
              )
            })}
          </div>
        </section>
      </div>
    </div>
  );
};
