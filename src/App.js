import React from 'react';
import './styles/main.scss';
import {RouterSwitch} from "./RouterSwitch";
import {BrowserRouter as Router} from 'react-router-dom';
import SimpleBar from 'simplebar-react';
import 'simplebar/dist/simplebar.min.css';

function App() {
  return (
    <div className="App">
      <SimpleBar forceVisible="y" style={{maxHeight: '100vh'}}>
        <Router>
          <RouterSwitch/>
        </Router>
      </SimpleBar>
    </div>
  );
}

export default App;
