export const SET_DATA = 'SET_DATA';

export const setData = (auth) => {
  return {
    type: SET_DATA,
    auth,
  }
};

export const LOGGED_OUT = 'LOGGED_OUT';
export const loggedOut = () => {
  return {
    type: LOGGED_OUT,
  }
};

export const CHANGE_NAME = 'CHANGE_NAME';

export const changeName = (name) => {
  return {
    type: CHANGE_NAME,
    name,
  }
}

