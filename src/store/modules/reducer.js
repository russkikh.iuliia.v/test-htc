import {SET_DATA, CHANGE_NAME, LOGGED_OUT} from './action';

const initialState = {
  authData: {},
};

export const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_DATA:
      return {
        ...state,
        authData: action.auth,
      };
    case LOGGED_OUT:
      return {
        ...state,
        authData: {loggedIn: false},
      };
    case CHANGE_NAME:
      return {
        ...state,
        authData: {...state.authData, "userName": action.name}
      }
    default:
      return state;
  }
};
