import { combineReducers } from 'redux';
import {authReducer} from "./modules/reducer";

export const rootReducer = combineReducers({
  auth: authReducer,
});
