import React, {useEffect, useState} from 'react';
import {createPortal} from 'react-dom';

export const Modal = (props) => {
  const {contentClasses = '', children, classes, isShown, closeModal, ...other} = props;
  const [active, setActive] = useState(false);

  useEffect(() => {
    setActive(() => isShown ? 'active' : false);
  }, [isShown]);

  return createPortal(
    <div className={`modal ${classes || ''} ${active || ''}`} {...other} >
      <div className='modal__overlay' onClick={closeModal}/>
      {isShown ?
        <div
          className={`modal__content${
            contentClasses ? ' ' + contentClasses : ''
          }`}
        >
          <button className='modal__close' type='button' onClick={closeModal}/>
          {children}
        </div>
        : <></>
      }
    </div>,
    document.getElementById('modal-root')
  )
}
