import React, {useRef} from 'react';
import {Logo} from "../Logo/Logo";
import {Search} from "../Search/Search";
import {Btn} from "../Btn/Btn";
import {Burger} from "../Burger/Burger";
import {User} from "../User/User";

export const Header = (props) => {

  const {showModalToggle, authHeader, loggedOut, userName, changeUserName} = props;

  const header = useRef(null);

  return (
    <header className='header' ref={header}>
      <div className="container">
        <div className="header__content">
          <Logo/>
          <Search className='header__form'/>
          {authHeader === 'true' ?
            <User changeUserName={changeUserName} userName={userName} loggedOut={loggedOut} className='header__user'/>
            :
            <div className="header__btn">
              <Btn tag='button' text='Войти' cn='btn_filled' type='button' onClick={showModalToggle}/>
            </div>
          }
          <Burger changeUserName={changeUserName} authHeader={authHeader} userName={userName} loggedOut={loggedOut}/>
        </div>
      </div>

    </header>
  )
};
