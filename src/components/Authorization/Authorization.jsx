import React, {useRef} from 'react';
import {Input} from "../Form/Input";
import {CheckboxInput} from "../Form/CheckboxInput";
import {Btn} from "../Btn/Btn";

export const Authorization = (props) => {
  const {submitHandler} = props;
  const login = useRef(null);
  const setLogin = (e) => {
    e.preventDefault()
    submitHandler(e, login.current.querySelector('input').value);
  };

  return (
    <div className="authorization">
      <span className="authorization__title">Вход</span>
      <form action="" className="authorization__form" onSubmit={setLogin}>
        <Input inputRef={login} placeholder='Логин' type='text' required classNames='authorization__input' pattern='^[a-zA-Z][a-zA-Z0-9-_\.]{3,20}$'/>
        <Input placeholder='Пароль' type='password' required classNames='authorization__input' pattern='[a-zA-Z0-9-_]{4,8}$'/>
        <CheckboxInput type='checkbox' placeholder='Запомнить' classNames/>
        <Btn tag='button' type='submit' text='Войти' cn='btn_filled authorization__btn'/>
      </form>
    </div>
  )
};
