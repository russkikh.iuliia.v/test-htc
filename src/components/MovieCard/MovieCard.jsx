import React from 'react';
import {Link} from "react-router-dom";

export const MovieCard = (props) => {
  const {
    title,
    description,
    img,
    alt
  } = props;
  return (
    <Link to='/' className='movie-card'>
      <span className="movie-card__poster">
        <img src={img} alt={alt}/>
        <span className='movie-card__description' dangerouslySetInnerHTML={{ __html: description }} />
      </span>
      <span className="movie-card__title" dangerouslySetInnerHTML={{ __html: title }} />
    </Link>
  );
};
