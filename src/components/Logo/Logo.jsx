import {ReactComponent as LogoIcon} from '../../assets/icons/logo.svg';
import {Link} from "react-router-dom";
import React from "react";

export const Logo = () => {
  return (
    <Link className="logo" to='./'>
      <LogoIcon />
      <span>Видеосервис</span>
    </Link>
    );
};
