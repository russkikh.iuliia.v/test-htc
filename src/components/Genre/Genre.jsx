import React from 'react';
import cn from 'classnames';
import {Link} from "react-router-dom";

export const Genre = (props) => {
  const {
    emoji,
    kind,
    bgColor
  } = props;

  let cardClasses = cn('genre', `genre_${bgColor}`);

  return (
    <Link to='/' className={cardClasses}>
      <span className="genre__emoji">{emoji}</span>
      <span className="genre__kind">{kind}</span>
    </Link>
  );
};
