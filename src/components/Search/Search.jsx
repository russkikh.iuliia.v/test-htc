import React from "react";
import {Input} from "../Form/Input";
import {Btn} from "../Btn/Btn";
import cn from 'classnames';

export const Search = (props) => {

  const {className} = props;

  const searchClass = cn('search', className);

  return (
    <form action="" className={searchClass}>
      <Input placeholder='Поиск...' classNames='search__input'/>
      <div className="search__btn">
        <Btn tag='button' text='Найти' cn='btn_texted' type='button'/>
      </div>
    </form>
  )
}
