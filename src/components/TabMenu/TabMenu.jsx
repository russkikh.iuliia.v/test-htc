import React, { useState, useRef } from 'react';
import {Link} from "react-router-dom";


export const TabMenu = (props) => {
  const {page} = props;
  const toChannels = useRef(null);
  const toHome = useRef(null);

  return (
    <div className='tab-menu'>
      <Link className={`tab-menu__item ${page === 'home' ? 'active' : ''}`} to='./' ref={toHome}>Фильмы</Link>
      <Link className={`tab-menu__item ${page === 'channels' ? 'active' : ''}`} to='./channels' ref={toChannels}>Телеканалы</Link>
    </div>
  );
};

