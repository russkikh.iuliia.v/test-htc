import React from 'react';
import cn from 'classnames';

export const Channel = (props) => {

  const {
    title,
    logo,
    items,
    className
  } = props;

  const channelClass = cn('channel', className);

  return (
    <div className={channelClass}>
      <div className="channel__logo">
        <img src={logo} alt=""/>
      </div>
      <div className="channel__info">
        <span className="channel__title" dangerouslySetInnerHTML={{ __html: title }} />
        <div className="channel__program">
          {items.map((show, index) => {
            return (
              <div className={index === 0 ? 'channel__item active' : 'channel__item'} key={index}>
                <span className="channel__time">{show.time}</span>
                <span className="channel__name" dangerouslySetInnerHTML={{ __html: show.name }} />
              </div>
            )
          })}
        </div>
      </div>
    </div>
  );
};

