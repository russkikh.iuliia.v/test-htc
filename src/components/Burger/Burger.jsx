import React, {useRef, useState} from 'react';
import {Logo} from "../Logo/Logo";
import {Search} from "../Search/Search";
import {User} from "../User/User";

export const Burger = (props) => {
  const {userName, loggedOut, authHeader, changeUserName} = props;

  const [hidden, setHidden] = useState(true);
  const burger = useRef(null);
  const burgerPanel = useRef(null);

  const setBurgerActive = () => {
    document.querySelector('body').style.overflow = 'hidden';
    setHidden(false);
    burger.current.classList.add('active');
  };

  const removeBurgerActive = () => {
    document.querySelector('body').style.overflow = '';
    burger.current.classList.remove('active');
    setTimeout(() => {
      setHidden(true)
    }, 200);
  };


  return (
    <div className="burger" ref={burger} tabIndex="-1">
      <div className="burger__trigger" onClick={setBurgerActive}/>
      <div className={`burger__panel ${hidden ? 'hidden' : ''}`} ref={burgerPanel}>
        <div className="container">
          <Logo/>
          <button className="burger__close" onClick={removeBurgerActive}/>
          <Search className='burger__form'/>
          {authHeader === 'true' ?
            <User changeUserName={changeUserName} userName={userName} loggedOut={loggedOut} className='burger__user'/>
            :
            <></>
          }
        </div>
      </div>
    </div>
  )
};
