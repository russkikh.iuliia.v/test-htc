import {Input} from "../Form/Input";
import React, {useEffect, useState, useRef} from "react";
import {Btn} from "../Btn/Btn";

export const User = (props) => {
  const {userName, loggedOut, className, changeUserName} = props;
  const inputRef = useRef(null);
  const [changeNameMood, setChangeNameMood] = useState(false);

  const unsetChangeNameMood = () => {
    setChangeNameMood(false);
  }

  const changeName = () => {
    setChangeNameMood(true);
  };

  useEffect(() => {
    if (inputRef.current) {
      inputRef.current.focus();
    }
  });

  return (
    <div className={`user ${className ? className : ''}`}>
      {!changeNameMood ?
        <span className="user__name"
              onClick={changeName}>{userName ? userName : ''}</span>
        :
        <form action="">
          <Input unsetChangeNameMood={unsetChangeNameMood} inputRef={inputRef} currentName={userName}
                 changeUserName={changeUserName} type='text'
                 placeholder='Имя пользователя'
                 classNames='user__change-name'
                 pattern='^[a-zA-Z][a-zA-Z0-9-_\.]{3,20}$'
          />
        </form>
      }
      <Btn tag='button' text='Выйти' cn='btn_texted' type='button' onClick={loggedOut}/>
    </div>
  )
}
