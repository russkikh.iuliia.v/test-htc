import React from 'react';
import { Modal } from '../Modal/Modal';
import {Authorization} from "../Authorization/Authorization";

export const AuthorizationModal = (props) => {
const {isShown, closeModal, submitHandler} = props;

  return (
    <Modal contentClasses="stop-modal__content" isShown={isShown} closeModal={closeModal}>
      <Authorization submitHandler={submitHandler} />
    </Modal>
  );
};
