import React from 'react';
import {ReactComponent as Logo} from '../../assets/icons/htc-logo.svg'

export const Footer = () => {
  return (
    <footer className="footer">
      <div className="container">
        <div className="footer__content">
          <div className="footer__icon">
            <Logo/>
          </div>
          <div className="footer__info">
            <span className="footer__text">426057, Россия, Удмуртская Республика, г. Ижевск, ул. Карла Маркса, 246 (ДК «Металлург»)</span>
            <div className="footer__text">
              <a href="tel:+7 (3412) 93-88-61, 43-29-29">+7 (3412) 93-88-61</a>,&nbsp;
              <a href="tel:43-29-29">43-29-29</a>
            </div>
            <a href="https://htc-cs.ru/" className="footer__link" target='_blank'>htc-cs.ru</a>
          </div>
        </div>
      </div>
    </footer>
  )
};
