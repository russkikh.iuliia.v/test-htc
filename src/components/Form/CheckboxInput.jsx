import React from 'react';
import {ReactComponent as Icon} from '../../assets/icons/checkboxOn.svg';
import cn from 'classnames';

export const CheckboxInput = (props) => {
  const {type, placeholder, classNames, ...other} = props;

  let labelClasses = cn('checkbox', classNames);

  return (
    <label className={labelClasses} {...other}>
      <input className='checkbox__input' type='checkbox' />
      <span className='checkbox__icon'>
        <Icon />
      </span>
      <span className='checkbox__placeholder'>{placeholder}</span>
    </label>
  );
};
