import React, {useState, useRef} from 'react';
import cn from 'classnames';

export const Input = (props) => {
  const {type, unsetChangeNameMood, placeholder, classNames, changeUserName, currentName, inputRef, ...other} = props;
  let [isFocused, setIsFocused] = useState(false);
  let [value, setValue] = useState('');
  const input = useRef(null);
  let labelClasses = cn('input', classNames, {
    input_focused: isFocused,
  });

  const makeFocused = () => {
    setIsFocused(true);
    input.current.value = currentName ? currentName : input.current.value;
    setValue(input.current.value);
  };
  const unFocused = () => {
    setIsFocused(value);
    if (changeUserName) {
      changeUserName(value);
    }
    if (unsetChangeNameMood) {
      unsetChangeNameMood();
    }
  };
  const handleInput = (e) => {
    setValue(e.target.value);
  };

  return (
    <label className={labelClasses} onFocus={makeFocused} onBlur={unFocused} ref={inputRef}>
      <input className='input__field' type={type} onInput={handleInput} {...other} ref={input}/>
      <span className='input__placeholder'>{placeholder}</span>
    </label>
  );
};
