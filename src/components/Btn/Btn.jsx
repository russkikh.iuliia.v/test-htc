import React from 'react';

export const Btn = (props) => {
  const {
    cn,
    text,
    tag,
    type,
    onClick
  } = props;

  const Tag = tag === 'link' ? 'Link' : `${tag}`

  return (
    <Tag className={`btn ${cn}`} type={type} onClick={onClick}>
      {text}
    </Tag>
  );
};

