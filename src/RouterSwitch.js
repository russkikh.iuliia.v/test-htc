import React, {useEffect, useState} from 'react';
import {Route, Switch} from 'react-router-dom';
import {Header} from "./components/Header/Header";
import {HomePage} from "./views/HomePage";
import {ChannelsPage} from "./views/ChannelsPage";
import {Footer} from "./components/Footer/Footer";
import {AuthorizationModal} from "./components/AuthorizationModal/AuthorizationModal";
import {useDispatch, useSelector} from "react-redux";
import {setData, loggedOut, changeName} from "./store/modules/action";

export const RouterSwitch = () => {

  const dispatch = useDispatch();

  let isLoggedIn = useSelector(state => state.auth.authData.loggedIn);
  let userName = useSelector(state => state.auth.authData.userName);

  let [isShown, setIsShown] = useState(false);
  const showModalToggle = () => {
    setIsShown(prevState => !prevState);
  };
  const closeModal = () => {
    setIsShown(false);
  };

  useEffect(() => {
    if (localStorage.getItem('loggedIn') === 'true') {
      dispatch(setData({
        "userLogin": "testLogin",
        "password": "testPassword",
        "userName": localStorage.getItem("userName") ? localStorage.getItem("userName") : 'Константин',
        "loggedIn": 'true'
      }));
    } else {
      dispatch(setData({
        "loggedIn": 'false',
      }));
    }
  }, []);

  const loggedOutHandler = () => {
    localStorage.setItem('loggedIn', 'false');
    dispatch(loggedOut());
  };

  const submitHandler = (e, name) => {
    e.preventDefault();
    dispatch(setData({
      "userLogin": "testLogin",
      "password": "testPassword",
      "userName": name,
      "loggedIn": 'true'
    }));
    closeModal();
    localStorage.setItem('loggedIn', 'true');
    localStorage.setItem('userName', name);
  };

  const changeUserName = (value) => {
    if (value) {
      dispatch(changeName(value));
      localStorage.setItem('userName', value);
    }
  }

  return (
    <>
      <div className='page'>
        <Header changeUserName={changeUserName} showModalToggle={showModalToggle} authHeader={isLoggedIn}
                loggedOut={loggedOutHandler} userName={userName}/>
        <div className="wrapper">
          <main>
            <div className="container">
              <Switch>
                <Route path="/channels">
                  <ChannelsPage/>
                </Route>
                <Route path="/">
                  <HomePage/>
                </Route>
              </Switch>
            </div>
          </main>
        </div>
        <Footer/>
        <AuthorizationModal isShown={isShown} closeModal={closeModal} submitHandler={submitHandler}/>
      </div>
    </>
  )
};
